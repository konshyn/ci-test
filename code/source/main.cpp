﻿#include <iostream>
#include <hello.h>

int main() 
{
    hello::greetings(std::cout) << std::endl;
    return 0;
}